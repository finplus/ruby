#!/usr/bin/env bash

# INSTALL ACL
sudo apt install acl

# CREATE AUDIT USER
echo "Enter user password: "
read user_password
[ -z "$user_password" ] && exit "User password cannot be blank."
sudo adduser audit --gecos "Audit User,RoomNumber,WorkPhone,HomePhone" --disabled-password
echo "audit:$user_password" | sudo chpasswd

# SET USER PERMISSION
echo "Enter code dir full path: "
read code_path
sudo setfacl -R -m u:audit:x $code_path

# LOGIN AS AUDIT USER
sudo su audit
mkdir .ssh
touch .ssh/authorized_keys

echo "ssh-rsa AAAAB3NzaC1yc2EAAAADAQABAAABAQCjc5bb2yOvg+AfTTWXPmlN9UvWQTJme4I3M/5BmiXokTpQVoCQAtm3driarO1h7yoFdwxiQVqdfgrBklKbTxDqgF0DViPnkqgWoi7nCHPMZknznr9qOS0YlwHAXj3B0mz/nijGl35oBTzs5OfkDUbZyEXGMbg2Wavd5QRKnNuEs/LgB8j+EQwrbNET8JnXQ7gDhAkeOVDBVsVPao9iFKu2vFmB+mFSD7CMb70K7HwmcrtMwhgKYroI+u6Jgua9KvxNdGKQUsT2DSVNLA1WABqIMApy/XyDErqCj2nvM+5jgjWqCR0uMH2HJOWVHPC7mInr0W1PWmPZRvFYeIlRBbht root@PCL-IT-CYPRIAN" >> .ssh/authorized_keys
echo "\nssh-rsa AAAAB3NzaC1yc2EAAAADAQABAAACAQDGtzhbohc2vLQdV4ADy1g4ST6nXVYQnP/13nWeLW0lhDDS6NRavQjhVRboxnXHOcYQ89hLlRwDGB6bkC9IwS0X/pm52zy498vWkdKLoJ53swbT+lKcww6gQ8oH6zac1rGlxjnyHflhEu5W9mbCnJ+KosSlYiHL4h6oUqrfaEGbfIG/jH3xgMFNDISdaJme03GVMp1MirWQxP0oFD5c2Qg8AhGE/Qbke1r3AT/eyYc1GG4lDaiLa/39VHLMyD4MAGpEeVoSTH1PUzR+YzChXtUcTHswGl9TBJtH4IxF4whFSP61J/RFQtaBVG/PKqtCE3vw1iCdnQFTnr2nEXICyKP1ZAAXAyK962ahmeJ7MnfcKb22r7vPCk8QMOc4OsOC1AQn5Ofdoh8cz+sLmCfNpBO3PNo3BabMPMulmyEmBqrft7qgHRZCVCgM+43XmkVfyMiC+9BjSPszVH2ZmiO38LwzYnutaF+rd5/K9Onxc4FY15b0ukQPs5lDJ+wgKfHJNZ01vm/TrP03Z55i8LfZIXru4B6JbKEOhpPgBYa7ByGNbb21VzD/nCiQFdUrUAfS0kCscVfgHKkdcmPz86vQubtUUGZ1TyWr2IG0ZbDrRu6YbuQ43Wuca7U7nFpAa+UYNrW066U8hg2TJbtp6biq8vSFdcIWHf0ne0iyhzQQfRHW0w== banta.bernard@gmail.com" >> .ssh/authorized_keys
