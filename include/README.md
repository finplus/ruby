# Description 

Configuration files that are used across all projects. 

| Filename | Description |
| :--- | :--- |
| .rspec | |
| .rubocop.yml | |
| .yardopts | |
| bitbucket-pipelines.yml | Bitbucket pipeline configuration |
