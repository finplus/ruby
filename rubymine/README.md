# Configure Ruby Mine to use FinPlus coding style 

To enable FinPlus coding formatting conformance in RubyMine, please follow the below steps;

1. Go to the main menu and open Files → Settings or use the shortcut (Ctrl + Alt + S)
2. Navigate to Editor → Code Style → Ruby 
3. Select the small gear icon next to "Scheme", select Import Scheme → IntelliJ IDEA code style XML
4. Select the FinPlus Code Style.xml. Select OK, then Apply, then OK
5. Ensure the FinPlus Code Style Scheme is selected

Good to go!!